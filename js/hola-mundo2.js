var map;
function Coordinates(lat, lng) {
    this.lat = lat;
    this.lng = lng;
}

function State(id, name) {
    this.id = id;
    this.name = name;
}

function Branch(name, address, stateId, lat, lng, imageUrl) {
    this.name = name;
    this.address = address;
    this.stateId = stateId;
    this.imageUrl = imageUrl;
    this.coordinates = new Coordinates(lat, lng);
}

/**
 * Contains all the available states
 * @type {Array}
 */
var states = [];

/**
 * Contains all the available branches
 * @type {Array}
 */
var branches = [];

$(inicializarEventos);

function inicializarEventos() {

    $.ajax({
        url: "api/states.php",
        context: document.body,
        dataType: 'json'
    }).done(function (data) {
        console.log(data);
        states = data;
        updateStatesSelect();
    });

    // init markers
    for (var i in branches) {
        var branch = branches[i];
        branch.marker = new google.maps.Marker({
            position: branch.coordinates,
            title: branch.name
        });

        branch.marker.addListener('click', function () {
            branch.infoWindow.open(map, this);
        });
    }

    // init info windows
    for (i in branches) {
        var branch = branches[i];
        branch.infoWindow = new google.maps.InfoWindow({
            content: 'Hello humans, ima info window'
        });
    }

    $('#select_state').change(function () {
        var stateId = $(this).val();
        $.ajax({
            url: "api/branches.php",
            dataType: 'json',
            data: {
                stateId: stateId
            }
        }).done(function (data) {
            branches = data;
            updateBranchesDOM();
        });
    });

    $('#select_state').change();

}

function updateStatesSelect() {
    $('#select_state').empty();
    // add all states option
    $('#select_state').append('<option value="0">Todos los estados</option>');
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        $('#select_state').append('<option value="' + state.id + '">' + state.name + '</option>');
    }
}

function updateBranchesDOM() {
    var branchesContainer = $('#states_info_container');
    branchesContainer.html('');

    for (var i in branches) {
        var newBranchNodes = createBranchNodes(branches[i]);
        branchesContainer.append(newBranchNodes);
    }
}

function updateBranchesMarkers() {
    for (var i in branches) {
        branches[i].marker.setMap(null);
    }

    var statesSelect = document.getElementById('select_state');
    var selectedStateId = statesSelect.options[statesSelect.selectedIndex].value;

    for (var i in branches) {
        if (selectedStateId == 0) {
            branches[i].marker.setMap(map);
        }
        else if (branches[i].stateId == selectedStateId) {
            branches[i].marker.setMap(map);
        }
    }
}

function createBranchNodes(branch) {
    // main container
    var well = $('<div class="well">');

    // image
    var image = $('<img class="img-responsive">');

    // title
    var title = $('<h3>' + branch.name + '</h3>');

    // direction
    var direction = $('<p>' + branch.address + '</p>');

    // add elements to main  container
    return well.append(image).append(title).append(direction);
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.266015, lng: -103.697682},
        zoom: 15
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(userAcceptsLocation);
    }
}

function userAcceptsLocation(position) {
    var initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    map.setCenter(initialLocation);
}