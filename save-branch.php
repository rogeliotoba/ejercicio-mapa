<?php

session_start();
if ($_SESSION['is_logged_in'] == false) {
    header('Location: login.php');
}

include "dao.php";

$data = array(
    "name" => $_REQUEST["name"],
    'state_id' => $_REQUEST["state_id"]
);

if (isset($_FILES['image']["name"])) {
    $target_file = basename($_FILES["image"]["name"]);
    $target_dir = "uploads/";
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    $info = getimagesize($_FILES["image"]["tmp_name"]);
    if ($info !== false) {
        // file is OK
        $newFileName = time() . rand(0, 100) . $_FILES["image"]["name"];
    }

    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        die('Tipo de imagen no soportado');
    }

    if(move_uploaded_file($_FILES['image']['tmp_name'], $target_dir . $newFileName)){
        $data['image'] = $newFileName;
    }
}

$result = db_insert_to_table("branches", $data);
if ($result) {
    header('Location: branches.php');
} else {
    die('Error al insertar en la base de datos');
}