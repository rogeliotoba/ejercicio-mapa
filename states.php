<?php
session_start();
if ($_SESSION['is_logged_in'] == false) {
    header('Location: login.php');
}

include "dao.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title>Todos los estados</title>
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        window.addEventListener('load', function () {
            var deleteBtns = document.getElementsByClassName('btn-delete-state');
            for (var i in deleteBtns) {
                var deleteBtn = deleteBtns[i];
                deleteBtn.addEventListener('click', function () {
                    var id = this.dataset.id;
                    var deleteBtn = document.getElementById('modal-btn-delete');
                    deleteBtn.setAttribute('href', 'delete-state.php?id=' + id);
                });
            }
        });
    </script>
</head>
<body>
<div class="container">
    <h1>Amin zone</h1>
    <h2>Estados</h2>
    <nav>
        <a href="admin.php" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Volver</a>
    </nav>

    <?php

    if ($_REQUEST['error-delete']) {
        ?>
        <div class="alert alert-warning fade in">
            <strong>Alerta!</strong> No se pude borrar el estado. Solo se puede borrar un estado
            cuando no tiene ninguna sucursal.
        </div>
        <?php
    }
    ?>

    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $estados = db_get_all('states');
        while ($estado = mysqli_fetch_array($estados)) {
            ?>
            <tr>
                <td><?= $estado['id'] ?></td>
                <td><?= $estado['name'] ?></td>
                <td>
                    <a href="#"
                       class="btn btn-danger btn-delete-state"
                       data-target="#modal-delete"
                       data-id="<?= $estado['id'] ?>"
                       data-toggle="modal">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
</div>


<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Confirmar eliminar</h4>
            </div>
            <div class="modal-body">
                <p>¿Estás seguro de eliminar el estado?</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default">Cancelar</button>
                <a class="btn btn-danger" id="modal-btn-delete" href="delete-state.php?id=0">Eliminar</a>
            </div>
        </div>
    </div>
</div>

</body>
</html>