<?php
session_start();
if($_SESSION['is_logged_in'] == false){
    header('Location: login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Crear Estado</title>
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <h1>Amin zone</h1>
    <h2>Crear estado</h2>
    <nav>
        <a href="admin.php" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Volver</a>
    </nav>
    <form class="form-horizontal" action="save-state.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Estado:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="name">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-danger">Agregar</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>