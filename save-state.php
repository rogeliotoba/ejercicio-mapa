<?php

session_start();
if ($_SESSION['is_logged_in'] == false) {
    header('Location: login.php');
}

include "dao.php";

$data= array("name"=>$_REQUEST["name"]);
$result = db_insert_to_table("states",$data);
if($result){
    header('Location: states.php');
}
else {
    die('Error al insertar en la base de datos');
}