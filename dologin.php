<?php

session_start();

include "dao.php";

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
$password = sha1($password);

$loginResult = validate_credentials($username, $password);

$user = mysqli_fetch_array($loginResult);
if ($user) {
    $_SESSION['is_logged_in'] = true;
    header('Location: admin.php');
} else {
    $_SESSION['is_logged_in'] = false;
    $_SESSION['messages'] = array('Credenciales incorrectas');
    header('Location: login.php');
}