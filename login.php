<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        body {
            background: #eee;
        }

        .form-container {
            background: #fff;
            padding: 1em;
            overflow: hidden;
        }

        h1, h2 {
            text-align: center;
        }

        h2 {
            padding-bottom: 0.5em;
            border-bottom: 1px solid #ccc;
            margin-bottom: 0.5em;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Sitio de motillos</h1>
    <h2>Login</h2>
    <?php
    if (isset($_SESSION['messages']) && count($_SESSION['messages']) > 0) {
        echo '<ul>';
        foreach ($_SESSION['messages'] as $message) {
            ?>
            <li><?= $message ?></li>
            <?php
        }
        echo '</ul>';
        $_SESSION['messages'] = array();
    }
    ?>

    <div class="form-container">
        <form method="POST" action="dologin.php">
            <div class="form-group">
                <label class="control-label">Username:</label>
                <input type="text" name="username" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">Password:</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
    </div>
</div>
</body>
</html>