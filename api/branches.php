<?php

include "../dao.php";

if (isset($_REQUEST['stateId'])) {
    // obtenemos todas las sucursales de ese estado
    $branches = db_get_where('branches', ['state_id' => $_REQUEST['stateId']]);
    $branchesResult = [];
    while ($branch = mysqli_fetch_array($branches)) {
        $branchesResult[] = $branch;
    }
    echo json_encode($branchesResult);

} else {
    // mostramos todas las sucursales
    $branches = db_get_all('branches');
    $branchesResult = [];
    while ($branch = mysqli_fetch_array($branches)) {
        $branchesResult[] = $branch;
    }
    echo json_encode($branchesResult);
}