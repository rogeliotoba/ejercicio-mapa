<?php

include "../dao.php";

if (isset($_REQUEST['stateId'])) {
    // get request state
    $stateResult = db_get('states', $_REQUEST['stateId']);
    echo json_encode(mysqli_fetch_array($stateResult));
} else {
    $states = db_get_all('states');
    $statesResult = [];
    while ($state = mysqli_fetch_array($states)) {
        $statesResult[] = $state;
    }
    echo json_encode($statesResult);
}