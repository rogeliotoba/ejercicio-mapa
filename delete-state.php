<?php
session_start();
if ($_SESSION['is_logged_in'] == false) {
    header('Location: login.php');
}

include "dao.php";

$result = db_delete_from_table('states', $_REQUEST['id']);
if ($result) {
    header('Location: states.php');
} else {
    header('Location: states.php?error-delete=true');
}